monolithic dialer features.

- blacklist a single number and list of numbers to upload. This works as local DND { do not disturb }
  * have option to search number
  * enable and disable DND option
- lead/data upload and auto dialing. Predictive Dialer feature.
- attendant tranfer, I think will see this later
- call back from already dialed number. will see later.
- call barge
- live monitoring
- call transfer
- call recording , in.ula and out.ula  ? or direct mp3  ?
- sip to sip call feature ?  option to enable and disable feature. ?
- cdr records
  * search call records against a number
  * filter option by date wise
  * filter option disposition wise
  * filter based on hangup cause from Provider like cause 21, 31 etc.  
    https://wiki.asterisk.org/wiki/display/AST/Hangup+Cause+Mappings
  * filter based on answered, busy, ringing, unreachable etc.
- voicemail ? I think not required for now.
- IVR
  * Customer can generate IVR by themself ?
  * upload recorded file
  * transfer in queue or on a mobile number if press zero or desired key
- queue
  * transfer in queue
  * music on hold
  * music in queue untill customer pick the call
  * how long the customre will be in queue ?
- incoming  
  - transfer on extension or number
- disposition report
- default crm provided by us
  * Box integeration with client CRM if they provide.
- twilio integeration, for pilvo or zentrunk will see later.

SIP authentication and add on's
 - sip enable/diable. Option to map the SIP for host IP address only ? So sip will register on mentioned IP address only.
 - select provider if have multiple provider. Set default provider ?
 - select DID before dial number
 - prefix for the campaign wise. Also have option to set default prefix for all campaign.

Box Admin Web Login
 - restart asterisk
 - restart tomcat
 - Box configuration like cpu, ram , disk space etc 


##### pre-install setup
```
OS : centos7.4 or 7.9 ?  
pacakges  
- jdk8
- asterisk-16.5.0
- default setup direcotry direcotry ?
```


##### JDK 8 install
```
sudo yum install wget -y
sudo mkdir /usr/lib/jvm/ -p
cd  /usr/lib/jvm/
wget https://s3.ap-south-1.amazonaws.com/custom-crais.xyz/jdk-8u241-linux-x64.tar.gz
sudo tar -xf jdk-8u241-linux-x64.tar.gz && rm jdk-8u241-linux-x64.tar.gz -rf
sudo update-alternatives --install /usr/bin/java java /usr/lib/jvm/jdk1.8.0_241/bin/java 1
sudo update-alternatives --install /usr/bin/javac javac /usr/lib/jvm/jdk1.8.0_241/bin/javac 1
# sudo update-alternatives --config java
# sudo update-alternatives --config javac
```


##### Asterisk install without menuselect GUI option
##### https://stackoverflow.com/questions/45824220/how-to-configure-asterisk-menuselect-build-options-programmatically
```
cd asterisk-16.5.0/
/bin/sh contrib/scripts/get_mp3_source.sh #This will download in the addon folder and will compile along with asterisk
sleep 4
./configure --with-jansson-bundled 
make menuselect.makeopts
menuselect/menuselect \
--enable chan_ooh323 \
--enable format_mp3 \
--enable res_config_mysql \
--enable app_macro \
--enable check_expr \
--enable check_expr2 \
--enable stereorize \
--enable smsq \
--enable streamplayer \
--enable CORE-SOUNDS-EN-ULAW \
--enable MOH-OPSOUND-ULAW \
--enable EXTRA-SOUNDS-EN-ULAW \
--disable CORE-SOUNDS-EN-GSM \
--disable MOH-OPSOUND-WAV \
menuselect.makeopts
make && make install && make samples && ldconfig
```


##### Asterisk install with default setup
```
cd /usr/src/ && \
wget https://downloads.asterisk.org/pub/telephony/asterisk/old-releases/asterisk-16.5.0.tar.gz && \
tar -xf asterisk-16.5.0.tar.gz && \
cd asterisk-16.5.0 && \
./contrib/scripts/install_prereq install && \
/bin/bash contrib/scripts/get_mp3_source.sh && \
./configure --with-jansson-bundled && \
make menuselect && \
make && make install && make samples && ldconfig && \
echo "Asterisk installed successfully."
touch /etc/asterisk/sip_custom.conf /etc/asterisk/extensions_custom.conf /etc/asterisk/queues_custom.conf && \
echo "#include "sip_custom.conf"" >> /etc/asterisk/sip.conf &&\
echo "#include "extensions_custom.conf"" >> /etc/asterisk/extensions.conf && \
echo "#include "queues_custom.conf"" >> /etc/asterisk/queues.conf
```


##### custom setup to migrate "/open-box"
```
# echo "30 17 * * * /sbin/shutdown -P now" >> /var/spool/cron/root
sed -ri 's/=enforcing|=permissive/=disabled/' /etc/selinux/config
sed -ri 's/=enforcing|=permissive/=disabled/' /etc/sysconfig/selinux

mkdir -p /open-box/deploy/asterisk/
mkdir -p /open-box/deploy/scripts/
mv /etc/asterisk /open-box/deploy/asterisk/etc-asterisk
mv /var/lib/asterisk /open-box/deploy/asterisk/var-lib-asterisk
mv /var/spool/asterisk /open-box/deploy/asterisk/var-spool-asterisk
ln -sf /open-box/deploy/asterisk/etc-asterisk /etc/asterisk
ln -sf /open-box/deploy/asterisk/var-lib-asterisk /var/lib/asterisk
ln -sf /open-box/deploy/asterisk/var-spool-asterisk /var/spool/asterisk
ln -sf /open-box/deploy/scripts /scripts

## setup asterisk startup service
# /sbin/asterisk >> /var/log/asteriskStartup.log
# echo "$(date +%F__%T)" >> /var/log/asteriskStartup.log
echo "/sbin/asterisk" >> /etc/rc.local
chmod 755 /etc/rc.d/rc.local

## create custom alias
echo "alias psast='ps aux | grep asterisk'" >> ~/.bashrc
# echo "alias astkill='/bin/kill -9 $(ps aux | grep asterisk| grep -v grep| awk '{print $2}')'" >> ~/.bashrc
source ~/.bashrc
asterisk
```


---
##### ssh key access
```
ssh-keygen
cp ~/.ssh/id_rsa.pub ~/.ssh/authorized_keys
# share ~/.ssh/id_rsa     ; privave key to the user. Note that login user name should be same user and key permission should be 400 if its linux OS.
                          ; if its on windos make putty compatible with puttykeygen
tar -cvf ssh-key.tar ~/.ssh                          ; can share .ssh complete directory if migrating.
# will add this in gcp metadata ?
```


##### backup script
```
mkdir /scripts/ && cd /scripts/ 
touch backup.sh && chmod u+x backup.sh
echo "00 17 * * * /bin/bash /scripts/backup.sh" >> /var/spool/cron/root
# need to add crontab , /script, /open-box/deploy  into backup script ?
```


##### backup.sh
```
#!/bin/bash
# this script will take complete backup of open-box

DAY=`date +%a`
DATE=`date +%F`

mkdir /backups/ && cd /backups/

if [[ $DAY == 'Thu' ]]; then
        cd /backups/
        tar -zcvf open-box-deploy-$DAY.tar.gz /open-box/deploy
        tar -zcvf open-box-deploy-$DATE.tar.gz open-box-deploy-*
        sleep 5
        rm -rf open-box-deploy-*
else
        cd /backups/
        tar -zcvf script-backup-$DAY.tar.gz /scripts
        tar -zcvf cronjob-backup-$DAY.tar.gz /var/spool/cron/root
        tar -zcvf open-box-deploy-$DAY.tar.gz /open-box/deploy
fi

```


---
##### SQLite basic commands
```
SQLite version 3.6.20 
Enter ".help" for instructions 
Enter SQL statements terminated with a ";" 
sqlite> .backup astCall-20200713.sql 
sqlite> .backup ast-20200713.sql 
sqlite> 
sqlite> 
```


---
##### Asterisk troubleshooting
```
ldd /usr/lib64/asterisk/modules/chan_dahdi.so                      ; check dahdi library and packages installed.  
ldmod| grep -iE dahdi                                              ; check dahdi drive loaded with kernel  
dahdi_genconf -vvvvv                                               ; load dahdi with kernel 
dahdi_cfg -vvvvv                                                   ; generate system.conf I think ?
```

---
##### Install freepbx 
https://wiki.freepbx.org/display/FOP/Installing+FreePBX+13+on+CentOS+6 


---
##### docker install
```
sudo yum install -y yum-utils
sudo yum-config-manager     --add-repo     https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install docker-ce docker-ce-cli containerd.io -y
sudo systemctl start docker
sudo docker run hello-world
```