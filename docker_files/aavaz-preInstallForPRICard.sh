#!/bin/bash
#Created by:Divya
# This will install Aavaz pre-install packages with setup of PRI card

ESC_SEQ="\x1b["
COL_BLUE=$ESC_SEQ"34;01m"
COL_RESET=$ESC_SEQ"39;49;00m"
RED=$ESC_SEQ"31;01m"

srvLoc="192.168.5.14:8001"
srcLocation="/usr/src/aavaz-preInstall"

preInstall(){
    cd $srcLocation/docker-src-Package/
    rpm -ivh --nodeps --force *
    sudo systemctl start docker   
    sudo docker run hello-world
    cd && wget http://192.168.5.14/aavaz-box/ab-install-plain-v3/-/blob/master/docker_files/dockerfile
    if [[ $? == 0 ]];
    then
	echo -e "$COL_BLUE \nAavaz pre-Installation is done. $COL_RESET"
    else
	echo -e "$RED \nAavaz pre-Installation is not done. Please contact Aavaz team. $COL_RESET"
    fi
}


dahdiDependency(){
    cd $srcLocation/rpm/
    rpm -ivh --nodeps --force *
    rpm -e --nodeps kernel-3.10.0-693.el7.x86_64 
    rpm -e --nodeps kernel-tools-libs-3.10.0-693.el7.x86_64 
    rpm -e --nodeps kernel-tools-3.10.0-693.el7.x86_64 
    rpm -e --nodeps kernel-headers-3.10.0-957.27.2.el7.x86_64 
    rpm -e --nodeps kernel-headers-3.10.0-1160.15.2.el7.x86_64
    echo;echo;
    echo   "##########################################################################################"
    echo   "#----------------------------------------------------------------------------------------#"
    echo   "#----------------------------------------------------------------------------------------#"
    echo   "#---------------------------------Please Reboot the system.------------------------------#"
    echo   "#----------------------------------------------------------------------------------------#"
    echo   "#----------------------------------------------------------------------------------------#"
    echo -e "########################################################################################## \n"

    sleep 10

}


ping -c 1 google.com
if [ $? == 0 ];then
    echo -e "$COL_BLUE \nInternet connectivity is fine, processing further. $COL_RESET"
    echo;echo; sleep 2
    cd /usr/src/
    #wget http://util.aavaz.biz/aavaz-preInstall.tar   
    wget http://$srvLoc/aavaz-preInstall.tar   
    tar -xvf aavaz-preInstall.tar
    preInstall
    dahdiDependency
else
    echo -e "$RED \nInternet connection is not established. Please contact to your Network team. $COL_RESET"
fi