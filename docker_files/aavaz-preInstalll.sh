#!/bin/bash 
#Created by:Divya
#This script will install the docker packages

ESC_SEQ="\x1b["
COL_BLUE=$ESC_SEQ"34;01m"
COL_RESET=$ESC_SEQ"39;49;00m"
RED=$ESC_SEQ"31;01m"

srvLoc="192.168.5.14:8001"
srcLocation="/usr/src/aavaz-preInstall/docker-src-Package"

preInstall(){
    cd $srcLocation
    rpm -ivh --nodeps --force *
    sudo systemctl start docker   
    sudo docker run hello-world
    cd && wget http://$srvLoc/aavaz-box/ab-install-plain-v3/-/blob/master/docker_files/dockerfile
    if [[ $? == 0 ]];
    then
	echo -e "$COL_BLUE \nAavaz pre-Installation is done. $COL_RESET"
    else
	echo -e "$RED \nAavaz pre-Installation is not done. Please contact Aavaz team. $COL_RESET"
    fi
}



ping -c 1 google.com
if [ $? == 0 ];then
    echo -e "$COL_BLUE \nInternet connectivity is fine, processing further. $COL_RESET"
    echo;echo; sleep 2
    cd /usr/src/
    #wget http://util.aavaz.biz/aavaz-preInstall.tar   
    wget http://$srvLoc/aavaz-preInstall.tar   
    tar -xvf aavaz-preInstall.tar
    preInstall
else
    echo -e "$RED \nInternet connection is not established. Please contact to your Network team. $COL_RESET"
fi
