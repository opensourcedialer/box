#!/bin/bash
# This will install dahdi with the packages of Sangoma or Digium card
ESC_SEQ="\x1b["
COL_BLUE=$ESC_SEQ"34;01m"
COL_RESET=$ESC_SEQ"39;49;00m"
RED=$ESC_SEQ"31;01m"

srvLoc="192.168.5.14:8001"
srcLocation="/usr/src/aavaz-preInstall"


dahdiInstall(){
	cd $srcLocation/
	tar -xvf $srcLocation/src-pkgs/dahdi-linux-complete-2.11.1.tar.gz
        echo;echo; sleep 2 
	cd dahdi-linux-complete-2.11.1+2.11.1
	make all && make install && make config
	if [[ $? == 0 ]];
	then 
		echo -e "Dahdi is installed." >> /var/log/box-installation.log
	else
		echo -e "$RED Dahdi is not installed. $COL_RESET" >> /var/log/box-installation.log
	fi

	#-------------------------------------------------------------------------------------------
        echo;echo; sleep 2 
	cd $srcLocation/
	tar -xvf $srcLocation/src-pkgs/libpri-1.5.0.tar.gz
	cd libpri-1.5.0/
	make && make install
	if [[ $? == 0 ]];
	then 
		echo -e "Libpri is installed." >> /var/log/box-installation.log
	else
		echo -e "$RED Libpri is not installed. $COL_RESET" >> /var/log/box-installation.log
	fi

}


sangomaSetup(){
	cd $srcLocation
	tar -xvf $srcLocation/src-pkgs/wanpipe-7.0.20.tgz
	cd wanpipe-7.0.20
        echo;echo;
	echo "********************* Please copy your DAHDI location: **********************"
	echo "$srcLocation/dahdi-linux-complete-2.11.1+2.11.1 - no slash at end"
	echo "You do not need this - just know where it is"
	echo "*****************************************************************************"
	sleep 10
        ln -sf $srcLocation/dahdi-linux-complete-2.11.1+2.11.1 /usr/src/zaptel
	./Setup install

	if [[ $? == 0 ]];
	then 
		echo -e "DahdiSangoma is installed." >> /var/log/box-installation.log
	else
		echo -e "$RED DahdiSangoma is not installed. $COL_RESET" >> /var/log/box-installation.log
	fi
}

digiumSetup(){
	dahdi_hardware         ## It shows the PRI card details
	dahdi_tool             ## Configure the dahdi tool.
	echo -e "span=1,1,0,ccs,hdb3,crc4\nbchan=1-15,17-31 \ndchan=16\nechocanceller=mg2,1-15,17-31\nloadzone  = us\ndefaultzone  = us" >> /etc/dahdi/system.conf
	
	dahdi_genconf          ##Load system.conf file.

	echo -e "options wcte13xp default_linemode=e1" >> /etc/modprobe.d/dahdi.conf
	## asterisk sholud be off and make sure dahdi modules are loaded.
	                     
	dahdi_cfg -v                   ## Configure dahdi at kernel level.
	dahdi_scan                     ## It check dahdi start properly or not. Alarm should be ok.
	dahdi_monitor 1 -vv            ## Monitor dahdi
	dahdi_test                     ## Test dahdi
}



ping -c 1 google.com ; echo $?
if [ $? == 0 ];then
    echo -e "$COL_BLUE \nInternet connectivity is fine, processing further.$COL_RESET"
    dahdiInstall
    cardType=`lspci | egrep "Digium|Sangoma" | awk -F':' '{print $3}' | head -c8 | paste -s -d" " | sed 's/ //g' | sed 's/,//g'`
    if [ "$cardType" == Sangoma ];then
        sangomaSetup
    else
        digiumSetup
   fi    

else 
    echo -e "$RED \nInternet connection is not established. Please contact to your Network team. $COL_RESET"
fi