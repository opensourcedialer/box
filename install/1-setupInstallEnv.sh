echo "Setting up environment to install the Aavaz Box"
ESC_SEQ="\x1b["
COL_BLUE=$ESC_SEQ"34;01m"
COL_RESET=$ESC_SEQ"39;49;00m"
RED=$ESC_SEQ"31;01m"
cd ..
installRoot=`pwd`
export installRoot
cd install

saneEnvironment() {
	echo "Checking and setting if basic settings and requirements are correct"
	#ln -sf /usr/share/zoneinfo/UTC /etc/localtime
	#cat /etc/sysconfig/clock
	#echo "ZONE=\"UTC\"" > /etc/sysconfig/clock
	timedatectl set-timezone UTC
	#Should check more things

	if [ -e "$installRoot/install/status" ]; then
		echo "status folder already exists"
	else
		mkdir $installRoot/install/status
		echo -e "Script - 1-setupInstallEnv.sh | Created status folder." >> /var/log/box-installation.log
	fi

}

createBuildDir() {

	if [ -e "$installRoot/build" ]; then
		echo "build folder already exists"
	else
		echo "Creating build directories"
		mkdir $installRoot/build
		mkdir $installRoot/build/tomcat
		mkdir $installRoot/build/asterisk
		mkdir $installRoot/build/other
		echo -e "Script - 1-setupInstallEnv.sh | Created build folder." >> /var/log/box-installation.log
	fi
}

createDeployDir() {

	FILE=sub-sys/aavazAppFullDirStructure

	if [ -e "/aavaz/deploy" ]; then
		echo "deploy directory /aavaz already exists"
	else
		echo "creating deploy direct /aavaz"
		for files in `cat $FILE`
		do
        	mkdir -p $files 
			echo "created: $files"
		done
		echo "make sure you delete /aavaz/box from the directory structure in the future"
		echo -e "Script - 1-setupInstallEnv.sh | Created /aavaz/deploy folder." >> /var/log/box-installation.log
	fi

}


setupNetwork() {

	if [ -e "$installRoot/install/status/1.networkDone" ]; then
		echo "Network already setup. Doing nothing"
	else
		echo "Disabling firewall and turning on chronyd"
       	/usr/bin/systemctl enable chronyd.service
		/usr/bin/systemctl start chronyd.service

		/usr/bin/systemctl disable firewalld
		/usr/bin/systemctl stop firewalld
		

		touch $installRoot/install/status/1.networkDone
		echo -e "Script - 1-setupInstallEnv.sh | Network setup and firewall disabled." >> /var/log/box-installation.log
	fi
}
echo
echo

saneEnvironment
createBuildDir
createDeployDir
setupNetwork

echo
echo "At the end of the install process make sure you run 99-cleanupInstall.sh"
echo
echo

