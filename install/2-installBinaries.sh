#!/bin/bash

ESC_SEQ="\x1b["
COL_RED=$ESC_SEQ"31;01m"
COL_RESET=$ESC_SEQ"39;49;00m"

echo 
echo 
echo 
echo "Installing repos from local"
echo
sleep 5
#READ FOLLOWING COMMENTS
# IF REQUIRED - Use following command to update xml-rpm-metadata as per rpm packages available in $installRoot/sys/bin-pkgs/rpm/
#  yum install -y --nogpgcheck createrepo
#  createrepo $installRoot/sys/bin-pkgs/rpm/
# IF REQUIRED 
cd $installRoot/install/
/bin/sh sub-sys/repoUseLocal.sh

#READ - Just install everything in the local repo without thinking - TO BE REVIEWED WITH RICHIE AS WE REQUIRE TO SWTCIH TO REPO DIRECTORY
cd $installRoot/sys/bin-pkgs/rpm/
rpm -ivh --nodeps --force *
#yum install -y --nogpgcheck *
cd $installRoot/
echo -e "Script - 2-installBinaries.sh | Aavaz packages installed." >> /var/log/box-installation.log
#READ - We probably need to match the exact kernel headers with the actual kernel (not the latest)
#yum install -y --nogpgcheck "kernel-devel-uname-r == $(uname -r)"
#READ - These lines below are for reference if you ever need to know what we actually installed
#yum install -y --nogpgcheck libtiff*
#yum install -y --nogpgcheck wget telnet rsync nc git java-devel xerces-j2 svn sqlite-devel SDL-devel gcc glib* zlib* patch bison gcc-c++ ncurses-devel flex sysstat lsof parted dosfstools traceroute ntp unzip automake* libtool* autoconf*
#yum install -y --nogpgcheck openssl-devel libxml2-devel unixODBC-devel libcurl-devel libogg-devel libvorbis-devel speex-devel spandsp-devel net-snmp-devel corosynclib-devel newt-devel popt-devel lua-devel postgresql-devel neon-devel libical-devel openldap-devel mysql-devel bluez-libs-devel gsm-devel libedit-devel libuuid-devel
#READ - Below not required - keeping for memory purposes -  EXCEPT gmime22-devel libsqlite3x-devel
#yum install -y spandsp-devel freetds-devel iksemel-devel radiusclient-ng-devel portaudio-devel libresample-devel sqlite2-devel jack-audio-connection-kit-devel #probably not required but check later
#yum groupinstall -y --nogpgcheck "Development Tools" # THOUGH Below is assumed to be the equivalent of group install
#yum install -y --nogpgcheck byacc cscope ctags cvs diffstat doxygen elfutils gcc-gfortran gettext indent intltool patchutils rcs redhat-rpm-config rpm-build swig systemtap gdb gettext-devel gettext-libs libgfortran systemtap-client systemtap-devel systemtap-runtime xz xz-lzma-compat
#yum install -y --nogpgcheck fuse  # For ntfs 
#yum install -y --nogpgcheck udev
#yum install -y alsa-lib-devel 
#yum install sox soxr

#cd $installRoot/sys/bin-pkgs/rpm
#rpm -ivh  ant-1.8.4-2.fc18.noarch.rpm
cd $installRoot/install/
#READ - REPEAT INSTALLATION FROM INTERNET FOR THE PACKAGES MISSING IN THE LOCAL REPOSITORY
/bin/sh sub-sys/repoUseNet.sh


echo
echo
echo "********** Extracting tomcat8 into /usr/share **********"
echo
sleep 5

cp $installRoot/sys/bin-pkgs/tgz/apache-tomcat-8.5.57.tar.gz /usr/share/
cd /usr/share/
tar -xvf apache-tomcat-8.5.57.tar.gz
mv apache-tomcat-8.5.57 tomcat8
rm -f apache-tomcat-8.5.57.tar.gz

#Add the ability to start and stop tomcat
cp  $installRoot/setup/res/tomcat8/tomcatinit /etc/init.d/tomcat
chmod 755 /etc/init.d/tomcat

# start and enable light httpd
sed -i 's/server.use-ipv6 = "enable"/server.use-ipv6 = "disable"/g' /etc/lighttpd/lighttpd.conf
systemctl restart lighttpd
systemctl enable lighttpd

# stop firewalld
systemctl disable firewalld
systemctl stop firewalld

#echo "System is going to reboot"
#sleep 30
#reboot
echo
echo
cd $installRoot/install/
echo   "##########################################################################################"
echo   "#----------------------------------------------------------------------------------------#"
echo   "#----------------------------------------------------------------------------------------#"
echo   "#----------Please Reboot the system. It is required for PRI card.------------------------#"
echo   "#----------------------------------------------------------------------------------------#"
echo   "#----------------------------------------------------------------------------------------#"
echo -e "########################################################################################## \n"

sleep 15
