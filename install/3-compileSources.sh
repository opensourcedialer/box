#!/bin/bash 
#Created by:Divya
ESC_SEQ="\x1b["
COL_BLUE=$ESC_SEQ"34;01m"
COL_RESET=$ESC_SEQ"39;49;00m"
RED=$ESC_SEQ"31;01m"

sqliteODBC() {

	cd $installRoot/build/asterisk	
	tar -xvf $installRoot/sys/src-pkgs/asterisk/sqliteodbc-0.9993.tar.gz
	cd sqliteodbc-0.9993
	./configure
	make && make install
	if [[ $? == 0 ]];
	then 
		echo -e "Script - 3-compileSources.sh | sqliteodbc is installed." >> /var/log/box-installation.log
	else
		echo -e "Script - 3-compileSources.sh | $RED sqliteodbc is not installed. $COL_RESET" >> /var/log/box-installation.log
	#lookup https://anukulverma.wordpress.com/2016/02/12/sqlite3odbc-installation/
	fi
}

commonInstall(){

	cd $installRoot/build/asterisk

	tar -xvf $installRoot/sys/src-pkgs/asterisk/dahdi-linux-complete-3.1.0+3.1.0.tar.gz
	cd dahdi-linux-complete-3.1.0+3.1.0
	make all && make install && make config
	if [[ $? == 0 ]];
	then 
		echo -e "Script - 3-compileSources.sh | Dahdi is installed." >> /var/log/box-installation.log
	else
		echo -e "Script - 3-compileSources.sh | $RED Dahdi is not installed. $COL_RESET" >> /var/log/box-installation.log
	#lookup https://anukulverma.wordpress.com/2016/02/12/sqlite3odbc-installation/
	fi

	#cat "$DIR2"sysconfig/dahdiconfE1 >> /etc/modprobe.d/dahdi.conf
	#modprobe wcte13xp default_linemode=e1; dahdi_genconf -v
	#cat "$DIR2"sysconfig/dahdisystem > /etc/dahdi/system.conf

	#-------------------------------------------------------------------------------------------
	cd $installRoot/build/asterisk
	tar -xvf $installRoot/sys/src-pkgs/asterisk/libpri-1.5.0.tar.gz
	cd libpri-1.5.0/
	make && make install
	if [[ $? == 0 ]];
	then 
		echo -e "Script - 3-compileSources.sh | Libpri is installed." >> /var/log/box-installation.log
	else
		echo -e "Script - 3-compileSources.sh | $RED Libpri is not installed. $COL_RESET" >> /var/log/box-installation.log
	#lookup https://anukulverma.wordpress.com/2016/02/12/sqlite3odbc-installation/
	fi

	#-------------------------------------------------------------------------------------------
	cd $installRoot/build/asterisk
	tar -xvf $installRoot/sys/src-pkgs/asterisk/gmime-2.2.27.tar.gz
	cd gmime-2.2.27
	./configure && make && make install
		
	if [[ $? == 0 ]];
	then 
		echo -e "Script - 3-compileSources.sh | Gmime is installed." >> /var/log/box-installation.log
	else
		echo -e "Script - 3-compileSources.sh | $RED Gmime is not installed. $COL_RESET" >> /var/log/box-installation.log
	fi
	cd ..
	sleep 5

	#-------------------------------------------------------------------------------------------
	#Installed lame 3.99.5 from yum
	#cd $installRoot/build/asterisk
	#tar -xvf $installRoot/sys/src-pkgs/asterisk/lame-3.99.5.tar.gz
	#cd lame-3.99.5
	#./configure && make && make install
	#sleep 5
	#cd ..

	#-------------------------------------------------------------------------------------------
	cd $installRoot/build/other
	tar -xvf $installRoot/sys/src-pkgs/sox/sox-14.4.2.tar.gz
	cd sox-14.4.2
	./configure
	make -s && make install
	ln -s /usr/local/bin/sox /usr/bin/sox
	ln -s /usr/local/bin/soxi /usr/bin/soxi
	if [[ $? == 0 ]];
	then 
		echo -e "Script - 3-compileSources.sh | sox is installed." >> /var/log/box-installation.log
	else
		echo -e "Script - 3-compileSources.sh | $RED Sox is not installed. $COL_RESET" >> /var/log/box-installation.log
	#lookup https://anukulverma.wordpress.com/2016/02/12/sqlite3odbc-installation/
	fi
	#Quick test
	soxi $installRoot/setup/res/other/pjsua/sample.mp3
	
	#-------------------------------------------------------------------------------------------
	cd $installRoot/build/other
	tar -xvf $installRoot/sys/src-pkgs/pjsip/pjproject-2.6.tar.bz2
	cd pjproject-2.6
	./configure --prefix=/usr/local --bindir=/usr/local/bin --sbindir=/usr/local/sbin --disable-libwebrtc --disable-video
	make dep && make && make install
	mkdir /root/bin
        \cp -r $installRoot/build/other/pjproject-2.6/ /root/bin/
	ln -s /root/bin/pjproject-2.6/pjsip-apps/bin/pjsua-x86_64-unknown-linux-gnu /usr/sbin/pjsua
		if [[ $? == 0 ]];
	then 
		echo -e "Script - 3-compileSources.sh | Pjsip is installed." >> /var/log/box-installation.log
	else
		echo -e "Script - 3-compileSources.sh | $RED Pjsip is not installed. $COL_RESET" >> /var/log/box-installation.log
	#lookup https://anukulverma.wordpress.com/2016/02/12/sqlite3odbc-installation/
	fi
}

customInstall() {
    #Compile the custom lsinfiles - for fast ls with custom output and filter on "-in." for recording conversions
    gcc -O3 $installRoot/sys/src-pkgs/lsulawfiles.c -o /usr/bin/lsulawfiles
	gcc -O3 $installRoot/sys/src-pkgs/lswavfiles.c -o /usr/bin/lswavfiles
}

asteriskInstall() {

	#-------------------------------------------------------------------------------------------
	cd $installRoot/build/asterisk
	rm -rf asterisk-16.5.0
	tar -xvf $installRoot/sys/src-pkgs/asterisk/asterisk-16-current.tar.gz
	cd asterisk-16.5.0/

	#echo "***** Applying patches for tel in SIP headers *****"
	#cp $installRoot/setup/res/asterisk/*.patch ./
	#patch channels/chan_sip.c chanSIP.patch 
	#patch channels/sip/reqresp_parser.c reqrespParser.patch

	echo "***** Getting mpg123 ******" 
	/bin/sh contrib/scripts/get_mp3_source.sh #This will download in the addon folder and will compile along with asterisk
	sleep 4

	./configure --with-jansson-bundled 
	make menuselect.makeopts

	menuselect/menuselect \
	--enable chan_ooh323 \
	--enable format_mp3 \
	--enable res_config_mysql \
	--enable app_macro \
	--enable check_expr \
	--enable check_expr2 \
	--enable stereorize \
	--enable smsq \
	--enable streamplayer \
	--enable CORE-SOUNDS-EN-ULAW \
	--enable MOH-OPSOUND-ULAW \
	--enable EXTRA-SOUNDS-EN-ULAW \
	--disable CORE-SOUNDS-EN-GSM \
	--disable MOH-OPSOUND-WAV \
	menuselect.makeopts
	# TODO - cp -v $installRoot/setup/res/asterisk/make/menuselect* ./ 
	make && make install && make samples && ldconfig
		if [[ $? == 0 ]];
	then 
		echo -e "Script - 3-compileSources.sh | Asterisk is installed." >> /var/log/box-installation.log
	else
		echo -e "Script - 3-compileSources.sh | $RED Asterisk is not installed. $COL_RESET" >> /var/log/box-installation.log
	#lookup https://anukulverma.wordpress.com/2016/02/12/sqlite3odbc-installation/
	fi
	
	#-------------------------------------------------------------------------------------------

}

DahdiSangoma(){
	cd $installRoot/build/asterisk
	tar -xvf $installRoot/sys/src-pkgs/asterisk/wanpipe-7.0.27.tar.gz
	cd wanpipe-7.0.27
	echo "********************* Please copy your DAHDI location: **********************"
	echo "$installRoot/build/asterisk/dahdi-linux-complete-3.1.0+3.1.0 - no slash at end"
	echo "You do not need this - just know where it is"
	echo "*****************************************************************************"
	ln -sf $installRoot/build/asterisk/dahdi-linux-complete-3.1.0+3.1.0 /usr/src/zaptel
	sleep 5
	./Setup dahdi --silent
	sleep 10
	wget http://util.aavaz.biz/pri.tar
 	tar -xvf pri.tar && cd pri
	cp chan_dahdi.conf /etc/asterisk/	
	cp system.conf /etc/dahdi/
	cp wanpipe1.conf /etc/wanpipe/
	pkill asterisk
	wanrouter restart
	dahdi_cfg -vvv
	asterisk


	if [[ $? == 0 ]];
	then 
		echo -e "Script - 3-compileSources.sh | DahdiSangoma is installed." >> /var/log/box-installation.log
	else
		echo -e "Script - 3-compileSources.sh | $RED DahdiSangoma is not installed. $COL_RESET" >> /var/log/box-installation.log
	#lookup https://anukulverma.wordpress.com/2016/02/12/sqlite3odbc-installation/
	fi
}

DahdiDigium(){
	dahdi_hardware         ## It shows the PRI card details
	dahdi_tool             ## Configure the dahdi tool.
	echo -e "span=1,1,0,ccs,hdb3,crc4\nbchan=1-15,17-31 \ndchan=16\nechocanceller=mg2,1-15,17-31\nloadzone  = us\ndefaultzone  = us" >> /etc/dahdi/system.conf
	
	dahdi_genconf          ##Load system.conf file.

	echo -e "options wcte13xp default_linemode=e1" >> /etc/modprobe.d/dahdi.conf
	## Also check /etc/udev/rules.d/dahdi.rules &  /etc/asterisk/chan_dahdi.conf
	## Run the below commands. 
	## asterisk sholud be off and make sure dahdi modules are loaded.
	                     
	dahdi_cfg -v                   ## Configure dahdi at kernel level.
	dahdi_scan
}

removeDahdiSangoma(){
	wanrouter modules  ##to check if any loaded
	wanrouter stop
        cd $installRoot/build/asterisk/wanpipe-7.0.20
        ./Setup remove
}

    

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------

echo "Starting install for asterisk and both required and sub-components"

cardType=`lspci | egrep "Digium|Sangoma" | awk -F':' '{print $3}' | head -c8 | paste -s -d" " | sed 's/ //g' | sed 's/,//g'`

cd $installRoot/sys/src-pkgs/asterisk

echo -e "$COL_BLUE \nPress Y to install sqliteODBC. $COL_RESET"
read -p "Install sqliteODBC? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    sqliteODBC
    echo -e "\n You successfully install sqliteODBC."
fi

echo -e "$COL_BLUE \nPress Y to insatll component. $COL_RESET"
read -p "Install common components? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
	commonInstall
	echo -e "\n You successfully install component."
fi

echo -e "$COL_BLUE \nPress Y to insatll custom components. $COL_RESET"
read -p "Install custom components developed in house? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
	customInstall
	echo -e "\n You successfully insatll custom components."
fi

echo -e "$COL_BLUE \nPress Y to install asterisk. $COL_RESET"
read -p "Install asterisk? This will be a manual step " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
	asteriskInstall
	echo -e "\n You successfully install asterisk."
fi

echo -e "$COL_BLUE \nPress Y to install PRI card. $COL_RESET"
read -p "Install $cardType? This will be manual step " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
	if [ "$cardType" == Sangoma ];then
		DahdiSangoma
  	  	echo 
		echo -e "\n You successfully configure Sangoma PRI card."
		cd $installRoot/install
       	fi

        if [ "$cardType" == Digium ];then
                DahdiDigium
				cd $installRoot/install
	        echo -e "\n You successfully configure Digium PRI card." 
        fi  
fi

#removeDahdiSangoma ############ "Run this to remove wanpipe configuration" ###################
echo
echo
