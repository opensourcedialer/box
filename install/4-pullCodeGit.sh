#!/bin/bash -x
#Created by:Divya

echo "Pulling code for tomcat and asterisk."
echo
echo
ESC_SEQ="\x1b["
COL_BLUE=$ESC_SEQ"34;01m"
COL_RESET=$ESC_SEQ"39;49;00m"
RED=$ESC_SEQ"31;01m"

echo -e "Pull Tomcat Apps? " 
read REPLY
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
        
	cd $installRoot/app/tomcat/antSrc/
	rm -rf $installRoot/app/tomcat/antSrc/*
	rm -rf $installRoot/app/vue/*
	rm -rf $installRoot/app/asterisk/*
        event=`cat /ab-install-plain-v3/versions/version.properties | grep -i event | awk -F'= ' $'{print $2}'`
	echo "Pulling the Event code from $event"
        sleep 2
	git clone -b $event http://aavazbox:1nstall123@192.168.5.14/aavaz-box/ab-v3bridge-events.git
	if [[ $? == 0 ]];
	then 
		echo -e "Script - 4-pullCodeGit.sh | V3bridge-events code is successfully pulled from branch $event." >> /var/log/box-installation.log
	else
		echo -e "Script - 4-pullCodeGit.sh | $RED V3bridge-events code is not pulled from branch $event. $COL_RESET" >> /var/log/box-installation.log
	fi	
	
	echo

	echo
	cd $installRoot/app/tomcat/antSrc/
        driver=`cat /ab-install-plain-v3/versions/version.properties | grep -i Driver | awk -F'= ' $'{print $2}'`
	echo "Pulling Driver code from $driver"
	git clone -b $driver http://aavazbox:1nstall123@192.168.5.14/aavaz-box/AB-V3Driver-BoxV3.git
	if [[ $? == 0 ]];
	then 
		echo -e "Script - 4-pullCodeGit.sh | AB-V3Driver-BoxV3 code is successfully pulled from branch $driver" >> /var/log/box-installation.log
	else
		echo -e "Script - 4-pullCodeGit.sh | $RED AB-V3Driver-BoxV3 code is not pulled from branch $driver. $COL_RESET" >> /var/log/box-installation.log
	fi	
	
	echo
	echo
 
	

	cd $installRoot/app/tomcat/antSrc/
        manage=`cat /ab-install-plain-v3/versions/version.properties | grep -i manage | awk -F'= ' $'{print $2}'`
	echo "Pulling managebox code from $manage"
	git clone -b $manage http://aavazbox:1nstall123@192.168.5.14/aavaz-box/ab-manage-v3.git
	if [[ $? == 0 ]];
	then 
		echo -e "Script - 4-pullCodeGit.sh | Manage code is successfully pulled from branch $manage." >> /var/log/box-installation.log
	else
		echo -e "Script - 4-pullCodeGit.sh | $RED Asterisk code is not pulled from branch $manage. $COL_RESET" >> /var/log/box-installation.log
	fi	
	
	echo
	echo

	cd $installRoot/app/tomcat/antSrc/
        api=`cat /ab-install-plain-v3/versions/version.properties | grep -i api | awk -F'= ' $'{print $2}'`
	echo "Pulling API code from $api"
	git clone -b $api http://aavazbox:1nstall123@192.168.5.14/aavaz-box/AB-Bridge-API-V3.git
	if [[ $? == 0 ]];
	then 
		echo -e "Script - 4-pullCodeGit.sh | AB-Bridge-API-V3 code is successfully pulled from branch $api." >> /var/log/box-installation.log
	else
		echo -e "Script - 4-pullCodeGit.sh | $RED AB-Bridge-API-V3 code is not pulled from branch $api. $COL_RESET" >> /var/log/box-installation.log
	fi	
	echo
	echo

	cd $installRoot/app/tomcat/antSrc/
        fastagi=`cat /ab-install-plain-v3/versions/version.properties | grep -i agi | awk -F'= ' $'{print $2}'`
	echo -e "Pulling the AB-FastAGI-V3 code from $fastagi \n"
	git clone -b $fastagi http://aavazbox:1nstall123@192.168.5.14/aavaz-box/AB-FastAGI-V3.git
	if [[ $? == 0 ]];
	then 
		echo -e "Script - 4-pullCodeGit.sh | AB-FastAGI-V3 code is successfully pulled from branch $fastagi." >> /var/log/box-installation.log
	else
		echo -e "Script - 4-pullCodeGit.sh | $RED AB-FastAGI-V3 code is not pulled from branch $fastagi. $COL_RESET" >> /var/log/box-installation.log
	fi	
	echo
	echo

	cd $installRoot/app/asterisk/
        ast=`cat /ab-install-plain-v3/versions/version.properties | grep -i asterisk | awk -F'= ' $'{print $2}'`
	echo "Pull the Aavaz-Box-Asterisk code from $ast"
	echo
	git clone -b $ast http://aavazbox:1nstall123@192.168.5.14/aavaz-box/ab-asterisk16.git
	if [[ $? == 0 ]];
	then 
		echo -e "Script - 4-pullCodeGit.sh | Asterisk code is successfully pulled from branch $ast." >> /var/log/box-installation.log
	else
		echo -e "Script - 4-pullCodeGit.sh | $RED Asterisk code is not pulled from branch $REPLY. $ast" >> /var/log/box-installation.log
	fi	
	echo
	echo
else
	echo -e "Script - 4-pullCodeGit.sh | $RED Tomcat and asterisk code are not pulled. $COL_RESET" >> /var/log/box-installation.log
fi

	cd $installRoot/setup
        script=`cat /ab-install-plain-v3/versions/version.properties | grep -i script | awk -F'= ' $'{print $2}'`
	git clone -b $script http://aavazbox:1nstall123@gitlab.aavaz.biz/aavaz/scripts-box.git
	if [[ $? == 0 ]];
	then 
		echo -e "Script - 4-pullCodeGit.sh | Scripts are successfully pulled from branch $script." >> /var/log/box-installation.log
	else
		echo -e "Script - 4-pullCodeGit.sh | $RED Scripts are not pulled from branch $script. $COL_RESET" >> /var/log/box-installation.log
	fi
	rm -rf scripts
	mv scripts-box/scripts .
	rm -rf scripts-box
	\cp -R /ab-install-plain-v3/setup/scripts/user/redeployment/* ~
	chmod +x ~/*
	echo "Pulled all scripts"
	echo


        mkdir -p $installRoot/app/vue
        cd $installRoot/app/vue
        vue=`cat /ab-install-plain-v3/versions/version.properties | grep -i vue | awk -F'= ' $'{print $2}'`
        git clone -b $vue http://aavazbox:1nstall123@192.168.5.14/aavaz-box/ab-manage-vue.git
        if [[ $? == 0 ]];
        then
                echo -e "Script - 4-pullCodeGit.sh | Aavazbox-vue are successfully pulled from branch $vue." >> /var/log/box-installation.log
        else
                echo -e "Script - 4-pullCodeGit.sh | $RED AavazBox-vue are not pulled from branch $vue. $COL_RESET" >> /var/log/box-installation.log
        fi
        echo
	
cd $installRoot/install
echo
echo

