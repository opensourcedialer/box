#!/bin/bash -x
#Created by:Divya
ESC_SEQ="\x1b["
COL_BLUE=$ESC_SEQ"34;01m"
COL_RESET=$ESC_SEQ"39;49;00m"
RED=$ESC_SEQ"31;01m"

copySourceToBuild(){
    \cp -r $installRoot/app/* $installRoot/build
    if [[ $? == 0 ]];
	then 
		echo -e "Script - 5-compileTomcatApps.sh | Copy source code to build." >> /var/log/box-installation.log
	else
		echo -e "Script - 5-compileTomcatApps.sh | $RED source code is not copied into the build folder. $COL_RESET" >> /var/log/box-installation.log
	fi
}

#-----------------------V3bridge-events---------------------
tomcatV3bridgeevents(){

    cd $installRoot/build/tomcat/antSrc/
    cd ab-v3bridge-events
    echo "***** Compiling ab-v3bridge-events source code *****"
    sleep 2
    /usr/bin/mvn compile
    /usr/bin/mvn install
    if [[ $? == 0 ]];
	then 
		echo -e "Script - 5-compileTomcatApps.sh | Code compiled - ab-v3bridge-events." >> /var/log/box-installation.log
	else
		echo -e "Script - 5-compileTomcatApps.sh | $RED Code is not compiled - ab-v3bridge-events. $COL_RESET" >> /var/log/box-installation.log
	fi
    echo "***** Finished compiling AavazBox-Manage source code *****"
}

#-----------------------AB-V3Driver-BoxV3---------------------
tomcatCompileV3DriverBoxV3(){

    #-----managebox context----

    cd $installRoot/build/tomcat/antSrc/
    cd AB-V3Driver-BoxV3
    echo "***** Compiling AavazBox-Manage (managebox context) source code *****"
    sleep 2
    /usr/bin/mvn compile
    /usr/bin/mvn install
    /usr/bin/mvn package -P boxProduction
    if [[ $? == 0 ]];
	then 
		echo -e "Script - 5-compileTomcatApps.sh | Code compiled - AB-V3Driver-BoxV3." >> /var/log/box-installation.log
	else
		echo -e "Script - 5-compileTomcatApps.sh | $RED Code is not compiled - AB-V3Driver-BoxV3. $COL_RESET" >> /var/log/box-installation.log
	fi
    echo "***** Finished compiling AavazBox-API source code *****"
}

#-----------------------Bridge-API-V3---------------------
tomcatCompileApi(){

    #-----managebox context----

    cd $installRoot/build/tomcat/antSrc/
    cd AB-Bridge-API-V3
    echo "***** Compiling Bridge-API-V3 source code *****"
    sleep 2
    /usr/bin/mvn compile
    /usr/bin/mvn install
    /usr/bin/mvn package -P boxProduction
    mv target/api-1.0-SNAPSHOT.war target/api.war
    if [[ $? == 0 ]];
	then 
		echo -e "Script - 5-compileTomcatApps.sh | Code compiled - Bridge-API-V3." >> /var/log/box-installation.log
	else
		echo -e "Script - 5-compileTomcatApps.sh | $RED Code is not compiled - Bridge-API-V3. $COL_RESET" >> /var/log/box-installation.log
	fi
    echo "***** Finished compiling AavazBox-Manage source code *****"
}


#-----------------------MANAGE BOX---------------------
tomcatCompileManage(){

    #-----managebox context----

    cd $installRoot/build/tomcat/antSrc/
    cd ab-manage-v3
    echo "***** Compiling AavazBox-Manage (managebox context) source code *****"
    sleep 2
    /usr/bin/mvn compile
    /usr/bin/mvn install
    /usr/bin/mvn package -P boxProduction
    \cp target/managebox-1.0-SNAPSHOT.war target/managebox.war
    \cp $installRoot/build/tomcat/antSrc/ab-manage-v3/ast* /aavaz/deploy/db/
    
    if [[ $? == 0 ]];
	then 
		echo -e "Script - 5-compileTomcatApps.sh | Code compiled - Managebox." >> /var/log/box-installation.log
	else
		echo -e "Script - 5-compileTomcatApps.sh | $RED Code is not compiled - Managebox. $COL_RESET" >> /var/log/box-installation.log
	fi
    echo "***** Finished compiling AavazBox-Manage source code *****"
}

#-----------------------Fastagi---------------------
tomcatCompileFastAGI(){

    #-----managebox context----

    cd $installRoot/build/tomcat/antSrc/
    cd AB-FastAGI-V3
    echo "***** Compiling AavazBox-Manage (managebox context) source code *****"
    sleep 2
    /usr/bin/mvn compile
    /usr/bin/mvn install
    /usr/bin/mvn package -P boxProduction
    mkdir -p /aavaz/deploy/dialplan/
    \cp target/AavazDialplan-1-jar-with-dependencies.jar target/AavazDialplan.jar 
    \cp target/AavazDialplan.jar /aavaz/deploy/dialplan/
    if [[ $? == 0 ]];
	then 
		echo -e "Script - 5-compileTomcatApps.sh | Code compiled - AB-FastAGI-V3." >> /var/log/box-installation.log
             java -jar /aavaz/deploy/dialplan/AavazDialplan.jar &>/dev/null &

	else
		echo -e "Script - 5-compileTomcatApps.sh | $RED Code is not compiled - AB-FastAGI-V3. $COL_RESET" >> /var/log/box-installation.log
	fi
    echo "***** Finished compiling AavazBox-Manage source code *****"
}

#-----------------------Boxv3-Vue---------------------
nodeCompileVue(){

    cd $installRoot/app/vue/
    cd ab-manage-vue
    echo "***** Compiling AavazBox-Vue source code *****"
    sleep 2
    \cp static/js/version.json static/js/version-original.json
    \cp /ab-install-plain-v3/versions/display_version.json static/js/version.json
    \cp /ab-install-plain-v3/setup/vue/config.js /ab-install-plain-v3/app/vue/ab-manage-vue/src/common/config.js 
    /usr/bin/npm install
    /usr/bin/npm run build
    sleep 2
    mkdir -p /var/www/lighttpd
    \cp dist/* /var/www/lighttpd/ -r 
     /usr/sbin/lighttpd
    if [[ $? == 0 ]];
        then
                echo -e "Script - 5-compileTomcatApps.sh | Code compiled - AB-Vue." >> /var/log/box-installation.log
        else
                echo -e "Script - 5-compileTomcatApps.sh | $RED Code is not compiled - AB-Vue. $COL_RESET" >> /var/log/box-installation.log
        fi
    echo "***** Finished compiling AavazBox-Manage source code *****"
}

echo
echo

read -p "Copy sources to build? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
	copySourceToBuild
fi

echo
read -p "Compile V3bridge-events? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
	tomcatV3bridgeevents
fi

echo
read -p "Compile V3Driver-BoxV3? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
	tomcatCompileV3DriverBoxV3
fi

echo
read -p "Compile API? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
       tomcatCompileApi
fi

echo
read -p "Compile Manage? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
	tomcatCompileManage
fi

echo
read -p "Compile FastAGI? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
	tomcatCompileFastAGI
fi

echo
read -p "Compile Vue? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
	nodeCompileVue
fi

cd $installRoot/install

