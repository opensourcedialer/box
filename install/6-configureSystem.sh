#!/bin/bash
#Created by:Divya
ESC_SEQ="\x1b["
COL_BLUE=$ESC_SEQ"34;01m"
COL_RESET=$ESC_SEQ"39;49;00m"
RED=$ESC_SEQ"31;01m"

backupFresh() {

	if [ -e "/etc/asterisk.fresh" ]; then
		echo "fresh asterisk backup already exists"
		echo -e "Script - 6-configureSystem.sh | Fresh asterisk backup already exists " >> /var/log/box-installation.log
	else
		echo "Making backups of fresh system"
		pkill asterisk
		mv /etc/asterisk /etc/asterisk.fresh
		echo -e "Script - 6-configureSystem.sh | Making backups of fresh system " >> /var/log/box-installation.log
		echo "Fresh asterisk install files are kept in /etc/asterisk.fresh"
	fi

}

#-----------------------------------------------+
#-----------------------------------------------|
# Copy databases to correct location            |
#-----------------------------------------------|
#-----------------------------------------------+

deployDB() {
	
	echo "If the database files are already there, you will be prompted to overwrite. Otherwise it will just place them."
	\cp /ab-install-plain-v3/app/tomcat/antSrc/ab-manage-v3/ast* /aavaz/deploy/db/
	echo "The following files are in /aavaz/deploy/db"
	ls /aavaz/deploy/db/
	if [[ $? == 0 ]];
	then 
		echo -e "Script - 6-configureSystem.sh | Database successfully copied. " >> /var/log/box-installation.log
	else
		echo -e "Script - 6-configureSystem.sh | $RED Database is not successfully copied. $COL_RESET" >> /var/log/box-installation.log
	fi	
}


#-----------------------------------------------+
#-----------------------------------------------|
# Install Asterisk application & files          |
#-----------------------------------------------|
#-----------------------------------------------+

configureAsterisk(){

	echo
	echo "Installing /etc/asterisk, /var/lib/asterisk/agi-bin, /var/lib/asterisk/sounds/custom/"
	mkdir -p /var/lib/asterisk/sounds/custom/
	mkdir -p /etc/asterisk
	\cp -rf $installRoot/app/asterisk/ab-asterisk16/config/var-ast/agi-bin/* /var/lib/asterisk/agi-bin/
	\cp -rf $installRoot/app/asterisk/ab-asterisk16/config/sounds/custom/*    /var/lib/asterisk/sounds/custom/
	\cp -rf $installRoot/app/asterisk/ab-asterisk16/config/etc-ast/*         /etc/asterisk/
       
	
	echo "Setting up in as the country and loadzone in indications.conf and system.conf"
	#These might already have been copied from the whole /etc/asterisk copy. This is just a double check
	sed -i -e "s/country=.*/country=in/"                               /etc/asterisk/indications.conf
	sed -i -re "s/loadzone=.*/loadzone=in/" -e "s/defaultzone=.*/defaultzone=in/" /etc/dahdi/system.conf

	echo "Setting up CDR files - These might no longer be used as we are doing this through AMI now"
	sed -i -e "/enable/s/^;//" -e "s/usegmtime=yes/usegmtime=no/"      /etc/asterisk/cdr.conf
	sed -i -e "s/enabled = no/enabled = yes/"                          /etc/asterisk/cdr_manager.conf #We do use this
	cat $installRoot/setup/res/asterisk/mappingcsv >>                  /etc/asterisk/cdr_custom.conf #We do use this
	if [[ $? == 0 ]];
	then 
		echo -e "Script - 6-configureSystem.sh | Asterisk is successfully configured. " >> /var/log/box-installation.log
	else
		echo -e "Script - 6-configureSystem.sh | $RED Asterisk is not successfully configured. $COL_RESET" >> /var/log/box-installation.log
	fi

	echo "Linking extensions.conf, ivr.conf, sip.conf"
	ln -sf /etc/asterisk/dialplan/extensions.conf /etc/asterisk/extensions.conf
	ln -sf /etc/asterisk/dialplan/extensions.conf.ivr /etc/asterisk/ivr.conf
	ln -sf /etc/asterisk/sipconfig/sip.conf.dahdi /etc/asterisk/sip.conf

}

configurePJSUA() {

	echo "Setting up default config files for pjsua in /root/ - NEED MORE HERE"
	\cp -rf $installRoot/setup/res/other/pjsua /root/
	if [[ $? == 0 ]];
	then 
		echo -e "Script - 6-configureSystem.sh | Pjsua setup is done. " >> /var/log/box-installation.log
	else
		echo -e "Script - 6-configureSystem.sh | $RED Pjsua setup is not done. $COL_RESET" >> /var/log/box-installation.log
	fi
}


configureTomcat(){

	echo "Making /usr/share/tomcat8/webapps/. All contexts will go under this location."
	mkdir -p /usr/share/tomcat8/webapps/
	rm -rf /usr/share/tomcat8/webapps/*
	mkdir -p  /usr/share/tomcat8/webapps/api
	mkdir -p /usr/share/tomcat8/webapps/managebox
	mkdir -p /usr/share/tomcat8/webapps/managebox/downloads #This location is used to first upload the IVR files then is processed further
        mkdir /usr/share/tomcat8/webapps/managebox/downloadLogs 
	if [[ $? == 0 ]];
	then 
		echo -e "Script - 6-configureSystem.sh | Tomcat directories successfully created. " >> /var/log/box-installation.log
	else
		echo -e "Script - 6-configureSystem.sh | $RED Tomcat directories are not successfully created. $COL_RESET" >> /var/log/box-installation.log
	fi

	echo "Updating /usr/share/tomcat8/bin/catalina.sh with JDK location and Memory options - using Minimum 300MB and Maximum 4GB"
	# adding lines 2,3,4 for java location and memory options
	sed -i '2i #' /usr/share/tomcat8/bin/catalina.sh
	sed -i '3i JAVA_HOME\="\/usr\/lib\/jvm\/jre-1.8.0-openjdk"' /usr/share/tomcat8/bin/catalina.sh
	sed -i '4i CATALINA_OPTS\="-Xms300M -Xmx4200M  -Djava.awt.headless=true -XX:+UseConcMarkSweepGC"' /usr/share/tomcat8/bin/catalina.sh

	# sed -i '/AccessLogValve/s/$/ resolveHosts="false"/'   /usr/share/tomcat8/conf/server.xml
	if [[ $? == 0 ]];
	then 
		echo -e "Script - 6-configureSystem.sh | Tomcat is successfully configured. " >> /var/log/box-installation.log
	else
		echo -e "Script - 6-configureSystem.sh | $RED Tomcat is not successfully configured. $COL_RESET" >> /var/log/box-installation.log
	fi
	service tomcat status
	sleep 2
}


#-----------------------------------------------+
#-----------------------------------------------|
# Install and configure Tomcat applications     |
#-----------------------------------------------|
#-----------------------------------------------+

deployTomcatApplications() {

	echo "Clearing /usr/share/tomcat8/webapps/managebox/ and copying manage context from build"
	cd $installRoot/build/tomcat/antSrc/ab-manage-v3/target
	rm -rf /usr/share/tomcat8/webapps/managebox/*
	\cp -rvf managebox.war  /usr/share/tomcat8/webapps/managebox/
        cd  /usr/share/tomcat8/webapps/managebox/
        unzip managebox.war
	if [[ $? == 0 ]];
	then 
		echo -e "Script - 6-configureSystem.sh | Managebox is successfully deployed. " >> /var/log/box-installation.log
	else
		echo -e "Script - 6-configureSystem.sh | $RED Managebox is not successfully deployed. $COL_RESET" >> /var/log/box-installation.log
	fi

	cd /ab-install-plain-v3/build/tomcat/antSrc/AB-Bridge-API-V3/target/
	rm -rf /usr/share/tomcat8/webapps/api/*
	\cp -rvf api.war              /usr/share/tomcat8/webapps/api/
        cd /usr/share/tomcat8/webapps/api/
        unzip api.war
	if [[ $? == 0 ]];
	then 
		echo -e "Script - 6-configureSystem.sh | API is successfully deployed. " >> /var/log/box-installation.log
	else
		echo -e "Script - 6-configureSystem.sh | $RED API is not successfully deployed. $COL_RESET" >> /var/log/box-installation.log
	fi
	#make sure you have copied the correct files in compile step

	echo "Creating the ROOT/index.html file so you can easily access manage context"
	mkdir /usr/share/tomcat8/webapps/ROOT/
	echo "<a href="managebox/">Manage your Aavaz Box</a>" > /usr/share/tomcat8/webapps/ROOT/index.html
}

#backupFresh
echo -e "$COL_BLUE \nPress Y to make a backup. $COL_RESET"
read -p "If this is a fresh system. Make a backup y or n? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
        echo
	backupFresh
        echo
        echo -e "\n You successfully took the backup"
fi

#deployDB
echo -e "$COL_BLUE \nPress Y to deploy database. $COL_RESET"
read -p "Deploy database file to /aavaz/deploy/db/? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
        echo
	deployDB
        echo
        echo -e "\n You successfully deploy tomcat."
fi

#configureTomcat
echo -e "$COL_BLUE \nPress Y to configure Tomcat. $COL_RESET"
read -p "Configure Tomcat? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then     
        echo  
        configureTomcat
        echo
        echo -e "\n You successfully configure configure tomcat."
fi

#deployTomcatApplications
echo -e "$COL_BLUE \nPress Y to deploy tomcat application. $COL_RESET"
read -p "Deploy tomcat application? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
        echo
	deployTomcatApplications
        echo
	echo -e "\n You successfully configure deploy tomcat application."
fi


#configureAsterisk
echo -e "$COL_BLUE \nPress Y to configure Asterisk. $COL_RESET"
read -p "Configure Asterisk? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
        echo
	configureAsterisk
        cd $installRoot/install
	echo	
	echo -e "\n You successfully configure asterisk."
fi

echo 
echo
cd $installRoot/install/
