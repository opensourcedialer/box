#!/bin/bash 
#Created by:Divya
ESC_SEQ="\x1b["
COL_BLUE=$ESC_SEQ"34;01m"
COL_RESET=$ESC_SEQ"39;49;00m"
RED=$ESC_SEQ"31;01m"

#----------------------------------------------------------------------------+
#----------------------------------------------------------------------------|
# Everything before this point is easy to reverse. Not so after this         |
#----------------------------------------------------------------------------|
#----------------------------------------------------------------------------+

#-----------------------------------------------+
#-----------------------------------------------|
# Package it all together - Caution - Moving    |
#-----------------------------------------------|
#-----------------------------------------------+

packageAavaz() {

	cp -rf $installRoot/setup/res       /aavaz/deploy/
	cp -rf $installRoot/setup/scripts   /aavaz/deploy/
	ln -sf /aavaz/deploy/scripts       /aavaz/scripts

	#-------------------
	# Tomcat
	#-------------------
	service tomcat stop
	
	mv /usr/share/tomcat8/conf         /aavaz/deploy/tomcat8/
	mv /usr/share/tomcat8/webapps      /aavaz/deploy/tomcat8/
	mv /usr/share/tomcat8/logs         /var/log/tomcat8/

	ln -sf /aavaz/deploy/tomcat8/webapps /usr/share/tomcat8/webapps
	ln -sf /aavaz/deploy/tomcat8/conf  /usr/share/tomcat8/conf
	ln -sf /var/log/tomcat8     /usr/share/tomcat8/logs


	# Application Logging and Properties

	mv /aavaz/deploy/res/common/*.properties /aavaz/deploy/common/
	mv /aavaz/deploy/res/common/*.properties.* /aavaz/deploy/common/
	mv /aavaz/deploy/res/common/chanunavail.message /aavaz/deploy/common/
	ln -s /aavaz/deploy/common/box.properties.prod /aavaz/deploy/common/box.properties
	

	rm -f /usr/share/tomcat8/webapps/managebox/WEB-INF/classes/log4j.properties
	mv /aavaz/deploy/common/log4j.properties.manage.prod /usr/share/tomcat8/webapps/managebox/WEB-INF/classes/log4j.properties.prod
	mv /aavaz/deploy/common/log4j.properties.manage.dev /usr/share/tomcat8/webapps/managebox/WEB-INF/classes/log4j.properties.dev
	\cp /usr/share/tomcat8/webapps/managebox/WEB-INF/classes/log4j.properties.prod /usr/share/tomcat8/webapps/managebox/WEB-INF/classes/log4j.properties
	ls -la /usr/share/tomcat8/webapps/managebox/WEB-INF/classes/log4j.properties
	if [[ $? == 0 ]];
	then 
		echo -e "Script - 7-packageSystem.sh | Managebox is successfully set up. " >> /var/log/box-installation.log
	else
		echo -e "Script - 7-packageSystem.sh | $RED Managebox is not successfully set up. $COL_RESET" >> /var/log/box-installation.log
	fi
	
	rm -f /usr/share/tomcat8/webapps/api/WEB-INF/classes/log4j.properties
	mv /aavaz/deploy/common/log4j.properties.api.prod /usr/share/tomcat8/webapps/api/WEB-INF/classes/log4j.properties.prod
	mv /aavaz/deploy/common/log4j.properties.api.dev /usr/share/tomcat8/webapps/api/WEB-INF/classes/log4j.properties.dev
	ln -s /usr/share/tomcat8/webapps/api/WEB-INF/classes/log4j.properties.prod /aavaz/deploy/common/log4j.properties
	\cp /usr/share/tomcat8/webapps/api/WEB-INF/classes/log4j.properties.prod /usr/share/tomcat8/webapps/api/WEB-INF/classes/log4j.properties
	ls -la /usr/share/tomcat8/webapps/api/WEB-INF/classes/log4j.properties
	if [[ $? == 0 ]];
	then 
		echo -e "Script - 7-packageSystem.sh | API is successfully set up. " >> /var/log/box-installation.log
	else
		echo -e "Script - 7-packageSystem.sh | $RED API is not successfully set up. $COL_RESET" >> /var/log/box-installation.log
	fi

	service tomcat start
	
	#-------------------
	# Asterisk
	#-------------------

	pkill asterisk
	
	mv /etc/asterisk                 /aavaz/deploy/asterisk/etc-asterisk
	mv /var/lib/asterisk             /aavaz/deploy/asterisk/var-lib-asterisk
	mv /etc/dahdi                    /aavaz/deploy/asterisk/etc-dahdi

	ln -sf /aavaz/deploy/asterisk/etc-asterisk     /etc/asterisk                 
	ln -sf /aavaz/deploy/asterisk/var-lib-asterisk /var/lib/asterisk             
	ln -sf /aavaz/deploy/asterisk/etc-dahdi        /etc/dahdi                    
	if [[ $? == 0 ]];
	then 
		echo -e "Script - 7-packageSystem.sh | Asterisk is successfully set up. " >> /var/log/box-installation.log
	else
		echo -e "Script - 7-packageSystem.sh | $RED Asterisk is not successfully set up. $COL_RESET" >> /var/log/box-installation.log
	fi

	if [ -e /var/spool/asterisk/monitor ]; then
		rm -rf /var/spool/asterisk/monitor
	fi
	ln -sf /dev/null /var/spool/asterisk/monitor #this means anything that gets saved here. Does not get saved.
	ll /var/spool/asterisk/monitor

	
	if [ -e /usr/share/tomcat8/webapps/managebox/recording ]; then
		rm -rf /usr/share/tomcat8/webapps/managebox/recording
	fi
	ln -sf /var/spool/asterisk/monitor  /usr/share/tomcat8/webapps/managebox/recording #This will be accessible via the web
	ll /usr/share/tomcat8/webapps/managebox/recording
	

	#cp -r "$DIR2"drivers/* /usr/local/lib/ #I believe we install the odbc driver from rpm
	
	\cp $installRoot/setup/res/asterisk/odbc* /etc/
	if [[ $? == 0 ]];
	then 
		echo -e "Script - 7-packageSystem.sh | ODBC is successfully set up. " >> /var/log/box-installation.log
	else
		echo -e "Script - 7-packageSystem.sh | $RED ODBC is not successfully set up. $COL_RESET" >> /var/log/box-installation.log
	fi
	#To test odbc
	#isql asterisk
	#select * from User;
	
	cp $installRoot/setup/res/asterisk/libsqlite3odbc.so /usr/lib64/
	echo "###### Make sure you find a home for the files in $installRoot/setup/res/asterisk/ #######"
	
	asterisk
}

#-----------------------------------------------+
#-----------------------------------------------|
# OS configurations                             |
#-----------------------------------------------|
#-----------------------------------------------+

configureOS()  {

	#export INIT=/etc/inittab
	#Make sure that the system is in run level 3 - not 5 which is for X11
	#export HWADDRVAL=`ifconfig -a | grep eth0 | awk -F' ' {'print $5'}`
	#export eth0IP=`ifconfig -a | grep eth0 -A1 | tail -n1 | awk '{print $2}'` 

	sed -ri 's/=enforcing|=permissive/=disabled/' /etc/selinux/config

	cd /aavaz/deploy/res/

	touch /etc/udev/rules.d/10-usb.rules
	ln -sf /aavaz/deploy/res/os/usbrules     /etc/udev/rules.d/10-usb.rules
	#ln -sf /usr/share/zoneinfo/Asia/Calcutta /etc/localtime
	ln -sf /usr/share/zoneinfo/UTC /etc/localtime
	cat /aavaz/deploy/res/os/rc_local >> /etc/rc.local
	echo "$eth0IP aavazbox aavazbox." >> /etc/hosts
	\cp /aavaz/deploy/res/os/limits.conf /etc/security/
	echo "fs.file-max =  20480" >> /etc/sysctl.conf

	\cp /aavaz/deploy/res/os/sqliterc /root/.sqliterc
	\cp /aavaz/deploy/res/os/bashrc   /root/.bashrc
	source /root/.bashrc

	mkdir -p /mnt/aavazRec

	##Support user create and give him particular access##
	useradd -d /home/support -m support
	echo "support:654321" | chpasswd	
	ln -s /aavaz/deploy/scripts/user/diag/ /home/support/diag-scripts
	chmod +t /aavaz -R
	echo "support ALL=(ALL) NOPASSWD: /usr/sbin/asterisk, /bin/netstat -tlnp, /sbin/ifconfig, /usr/bin/crontab, /bin/ps, /usr/sbin/pjsua, /usr/bin/sqlite3, /usr/bin/tail, /sbin/service, /usr/bin/pkill, /bin/kill" >> /etc/sudoers
	echo "%wheel  ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers

    cd /aavaz/deploy
    git init
    echo -e "Script - 7-packageSystem.sh | OS is successfully set up. " >> /var/log/box-installation.log
	
}

#-----------------------------------------------+
#-----------------------------------------------|
# Setup system monitoring, logs and management  |
#-----------------------------------------------|
#-----------------------------------------------+

configureLogMonMan(){

	#--------------------------------+
	# log configurations             |
	#--------------------------------+
	touch /etc/logrotate.d/asterisk
	touch /etc/logrotate.d/tomcat8
	cat $installRoot/setup/res/asterisk/asterisklog > /etc/logrotate.d/asterisk
	cat $installRoot/setup/res/tomcat8/tomcatlog > /etc/logrotate.d/tomcat8
	
	#--------------------------------+
	# sys management scripts         |
	#--------------------------------+

	cat /aavaz/deploy/res/os/cron > /var/spool/cron/root
	echo -e "Script - 7-packageSystem.sh | Aavaz logrotate is successfully set up. " >> /var/log/box-installation.log
	#later

}

makeFactoryImage() {

	echo "Updating the time stamp on all files in deploy to now"
	find /aavaz -exec touch -m {} \;
	echo "Copying everything to the factory folder"
	rsync -avz /aavaz/deploy/ /aavaz/factory/
	date >> /aavaz/factory/createdOn
	cd /aavaz/factory
	ll -R > factoryInstall.list
	echo "Added README.txt if you need to compare in the future"
	echo "cd to /aavaz/deploy" > README.txt
	echo "ll -R > currentDeploy.list" >> README.txt
	echo "diff currentDeploy.list ../factory/factoryInstall.list" >> README.txt
	echo -e "Script - 7-packageSystem.sh | Aavaz factory image is successfully created. " >> /var/log/box-installation.log
}

reconfigureNetwork() {

	#The fixed administrator IP is on eth0 - default at setup is 192.168.15.15
	#The configurable box IP is on eth0:0 - default at setup is 192.168.5.222	
	echo "need to reboot system to get updated eth0 IP"

	mv /etc/sysconfig/network-scripts/ifcfg-eth0 /etc/sysconfig/network-scripts/ifcfg-eth0.bak
	mv /etc/sysconfig/network-scripts/ifcfg-eth0\:0 /etc/sysconfig/network-scripts/ifcfg-eth0\:0.bak

	ln -sf /aavaz/deploy/res/os/ifcfg-eth0   /etc/sysconfig/network-scripts/ifcfg-eth0
	ln -sf /aavaz/deploy/res/os/ifcfg-eth0\:0 /etc/sysconfig/network-scripts/ifcfg-eth0\:0
	ln -sf /aavaz/deploy/res/os/network      /etc/sysconfig/network 

	eth0IP=`grep IPADDR /etc/sysconfig/network-scripts/ifcfg-eth0 | awk -F= '{print $2}'`
	eth0IPOrig=`grep aavazbox. /etc/hosts | awk '{print $1}'`
	sed -i '/aavazbox\./s/"$eth0IPOrig"/"$eth0IP"/' /etc/hosts
        echo -e "Script - 7-packageSystem.sh | Network is successfully set up. " >> /var/log/box-installation.log
  	#service network restart
}

readyForTest() {

#Run me as a function

	#ASTERISK_URL=CHANGEME-ASTERISK-IP
	read -p "System IP Address? " -r
	sed -i -e "/^ASTERISK_URL/s/=.*$/=$REPLY/" /aavaz/deploy/common/box.properties.prod
	sed -i -e "/BOXIP/s/=.*$/=$REPLY/" /etc/asterisk/extensions.conf
        sed -i -e "/IPADDR/s/=.*$/=$REPLY/" /etc/sysconfig/network-scripts/ifcfg-eth0\:0
        read -p "System GATEWAY Address? " -r 
        sed -i -e "/GATEWAY/s/=.*$/=$REPLY/" /etc/sysconfig/network-scripts/ifcfg-eth0\:0
        read -p "System NETMASK Address? " -r
        sed -i -e "/NETMASK/s/=.*$/=$REPLY/" /etc/sysconfig/network-scripts/ifcfg-eth0\:0

	#CLIENT_URL=CHANGEME-CLIENT-URL
	echo
	read -p "Client CAB URL (url).aavaz.biz ? " -r
	sed -i -e "/^CLIENT_URL/s/=.*$/=$REPLY.aavaz.biz/" /aavaz/deploy/common/box.properties.prod

	#USER_NAME=CHANGEME-CLIENT-USER-NAME
	read -p "Client CAB User Name? " -r
	sed -i -e "/^USER_NAME/s/=.*$/=$REPLY/" /aavaz/deploy/common/box.properties.prod

	#PASSWORD=CHANGEME-CLIENT-PASSWORD
	read -p "Client CAB Password? " -r
	sed -i -e "/^PASSWORD/s/=.*$/=$REPLY/" /aavaz/deploy/common/box.properties.prod

changeMode(){

        #CALLING_MODE can be one of three modes twilio, DAHDI, SIP/providertrunk
        #CALLING_MODE = DAHDI
        echo "Which PBX(calling mode) you are using?"
        echo -e "1)Press 1 for PRI line"
        echo -e "2)Press 2 for GSM gateway."
        read
        if [[ $REPLY = "1" ]]
        then
                sed -i -e "/^CALLING_MODE/s/=.*$/= DAHDI/" /aavaz/deploy/common/box.properties.prod
                sed -i -e "/ISMULTILINE /s/=.*$/= FALSE/" /etc/asterisk/extensions.conf
        else
                sed -i -e "/CALLING_MODE/s/=.*$/= SIP\/PROVIDERTRUNK/" /aavaz/deploy/common/box.properties.prod
                sed -i -e "/ISMULTILINE /s/=.*$/= TRUE/" /etc/asterisk/extensions.conf
        fi

}
changeMode

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#OUTBOUNDTRUNKGROUP = Dahdi/g1
#TRANSFERSTATE=SIP
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
echo -e "Script - 7-packageSystem.sh | Aavaz client details is successfully set up on Aavaz-Box. " >> /var/log/box-installation.log

}
echo -e "$COL_BLUE \n!!!!Be Alert!!! $COL_RESET"
read -p "!!!!! From this point on it will be hard to go back. OK y/n? " -n 1 -r
read REPLY
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
	echo "----------------OK------------Say Yes Carefully-----------"
fi



#packageAavaz
echo -e "$COL_BLUE \nPress Y to configure Aavaz. $COL_RESET"
read -p "Package Aavaz - Will move everything to the /aavaz/deploy folder and create links back to it? y/n " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
	echo
	packageAavaz
	echo -e "\n You successfully configure aavaz."

fi

#configureOS
echo -e "$COL_BLUE \nPress Y to configure OS. $COL_RESET"
read -p "Configure the OS? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
	echo
	configureOS
	echo
	echo -e "\nYou successfully configure OS."
fi

#configureLogMonMan
echo -e "$COL_BLUE \nPress Y to configure LogMonMan. $COL_RESET"
read -p "Configure Logging, Monitoring and Management? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
	echo
	configureLogMonMan
	echo
	echo -e "\nYou successfully configure Logging, Monitoring and Management."
fi

#makeFactoryImage
echo -e "$COL_BLUE \nPress Y to make Factory Image. $COL_RESET"
read -p "Make Factory Image? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
	makeFactoryImage
        echo
        echo -e "\nYou successfully made the factory image."
fi

#configurePJSUA
echo -e "$COL_BLUE \nPress Y to configure PJSUA. $COL_RESET"
read -p "PJSUA? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
	echo
	configurePJSUA
	echo
 	echo -e "\nYou successfully configure PJSUA."

fi

##reconfigureNetwork
#echo -e "$COL_BLUE \nPress Y to reconfigure the network. $COL_RESET"
#read -p "Reconfigure the network? " -n 1 -r
#echo    # (optional) move to a new line
#if [[ $REPLY =~ ^[Yy]$ ]]
#then
#	echo
#	reconfigureNetwork
#	echo	
#	echo -e "\nYou successfully reconfigure network."
#fi

#echo -e "$COL_BLUE \nPress Y to make ready system for testing. $COL_RESET"
#read -p "Ready system for testing? You can run this as many times as you want. y/n?" -n 1 -r
#echo    # (optional) move to a new line
#if [[ $REPLY =~ ^[Yy]$ ]]
#then
#	echo
#	readyForTest
#	cd $installRoot/install
#	echo
#	echo -e "\nYou successfully ready the system for testing."
#fi

echo
echo

echo "You have successfully installed the Aavaz-BoxV3."
cd $installRoot/install/
