#!/bin/bash

cleanBuildStatus(){

	echo
	echo "Cleaning $installRoot/build folder"
	echo
	rm -rf $installRoot/build
	echo
	echo "Cleaning $installRoot/status folder"
	echo
	rm -rf $installRoot/install/status

}

cleanPulledGitSources(){

	echo
	echo "Cleaning $installRoot/app/tomcat/antSrc/ and $installRoot/app/asterisk/"
	echo
	echo
	rm -rf $installRoot/app/tomcat/antSrc/*
	rm -rf $installRoot/app/asterisk/*

}

invalid(){

	echo "Invalid selection. y or n only. exiting."
	exit
}

read -p "Clean build directory (y/n) ? " choice
case "$choice" in 
  y|Y ) cleanBuildStatus;;
  n|N ) echo "Please make sure you don't push to git with build folder and additional sources";;
  * ) invalid;;
esac

read -p "Clean additional pulled git sources (y/n) ? " choice
case "$choice" in 
  y|Y ) cleanPulledGitSources;;
  n|N ) echo "Please make sure you don't push to git with additional sources";;
  * ) invalid;;
esac

