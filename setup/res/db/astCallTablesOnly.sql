CREATE TABLE "Call"(id integer primary key autoincrement,contactID int(11),
callerID int(11),campaignID int(11),assignmentID int(11),providerID int(11),
callUID varchar(100),timeOfCall datetime,notes varchar(20000),dialResult varchar(255),
lead bit(1),dialResultDetails varchar(1000),callType varchar(100),callStatus varchar(500),
phoneNumber varchar(255),providerDailingMode varchar(255), `callTransferCallerID` int(11) DEFAULT NULL,
          `queueName` varchar(250) DEFAULT NULL, `ivrName` varchar(250) DEFAULT NULL,
          `ivrID` int(11) DEFAULT NULL,
          `isio` char(1) DEFAULT 'O');
          
CREATE TABLE `CallDetail` (
                        id integer primary key autoincrement,
                         keyword varchar(255) DEFAULT NULL,
                        value varchar(1000) DEFAULT NULL,
                        callUID varchar(100) DEFAULT NULL,
                        callEndState varchar(200) DEFAULT NULL,
                        callTime varchar(100) DEFAULT NULL
                           );
                           
CREATE TABLE `CallEvent` (
 id integer primary key autoincrement, CUID varchar(256),callerID varchar(150),atTime varchar(150),event varchar(150),keyword varchar(250),value varchar(250)
);

CREATE TABLE "CallTX" (
 id integer primary key autoincrement, CUID varchar(256),messageID varchar(256)
);

CREATE TABLE `PCDTX` (
 id integer primary key autoincrement, CUID varchar(256),messageID varchar(256)
);

CREATE TABLE "ProviderCallDetail" (
                       `id` integer primary key autoincrement,
                       `providerCallSID` varchar(256) ,
                       `contactID` int(11) ,
                       `callerID` int(11) ,
                       `campaignID` int(11) ,
                       `providerID` int(11) ,
                       `callUID` varchar(100) ,
                       `providerCallStatus` varchar(100) ,
                       `providerRecordingUrl` varchar(256) ,
                       `providerCallTimeSpent` varchar(100) ,
                       `callID` int(11) ,
                       `phoneNumber` varchar(20) ,
                       `timeOfCall` datetime ,
                       `callStatus` bit(1) default '\0',
                       `callType` varchar(50) ,
                       `providerDailingMode` varchar(256) ,
                       `DialBLegUUID` varchar(100) ,
                       `DialBLegBillRate` varchar(20) ,
                       `DialBLegHangupCause` varchar(100) ,
                       `DialALegUUID` varchar(100) ,
                       `EndTime` varchar(100) ,
                       `DialBLegTo` varchar(100) ,
                       `DialBLegDuration` varchar(20) ,
                       `CallUUID` varchar(100) ,
                       `AnswerTime` varchar(100) ,
                       `DialBLegTotalCost` varchar(100) ,
                       `DialAction` varchar(100) ,
                       `DialBLegFrom` varchar(100) ,
                       `DialBLegPosition` varchar(20) ,
                       `DialBLegBillDuration` varchar(100) ,
                       `DialBLegStatus` varchar(100) ,
                       `StartTime` varchar(20) ,
                       `Event` varchar(100) ,
                       `isMachineDetected` bit(1) default '\0'

                     , isio char(1));
                     