# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

# User specific aliases and functions
alias c='clear'
alias d='pushd'
alias p='popd'
alias r='pushd +1'
alias ls='\ls -F'
alias rm='rm -i'
alias rmf='rm -f'
alias mount='mount |column -t'
alias ping='ping -c 10 -s.5'
alias ports='netstat -tulanp'
## pass options to free ##
alias meminfo='free -m -l -t'

alias mv='mv -i'
#alias vi=vim

alias ll='\ls -alhF --color'
alias psjava='ps -ef | grep java'
alias psast='ps -ef | grep asterisk'
alias tailc='tail -f /var/log/tomcat7/catalina.out'
alias disk-space-usage='du -skh *'
alias disk-space-fs='df -kh'

## get top process eating memory
alias psmem='ps auxf | sort -nr -k 4'
alias psmem10='ps auxf | sort -nr -k 4 | head -10'

## get top process eating cpu ##
alias pscpu='ps auxf | sort -nr -k 3'
alias pscpu10='ps auxf | sort -nr -k 3 | head -10'

## Get server cpu info ##
alias cpuinfo='lscpu'

## older system use /proc/cpuinfo ##
##alias cpuinfo='less /proc/cpuinfo' ##

## Asterisk

mergeAllUlaw2MP3 () {
RECDIR="/var/spool/asterisk/monitor"
        
if [ "$1" = "" ] ; then
    echo "Usage: mergeUlaw2mp3 [file-xyz.ulaw|file-xyz.ul]. Will leave file-in.ul, file-out.ul and file.mp3"
    return 4
fi

fileIn=$1
#Bash String Manipulation http://www.thegeekstuff.com/2010/07/bash-string-manipulation/
fileName=${fileIn%-*} #Delete everything match in reverse  "-*"  ${string%substring}
fileExt=${fileIn##*.} #Delete till the last occurrence of matching "*." ${string##substring}

if [ -f $fileName"-in."$fileExt ] && [ -f $fileName"-out."$fileExt ]; then
	echo "Both in and out files exist. Continuing."
	totalFile=`ls | wc -l`
	echo -e "Total file for conversion is $totalFile"
else
	echo "Error: One of the in or out files do not exist. Exiting."
return 4
fi

if  [ "$fileExt" = "ulaw" ]; then
	read -p "The mergeAllUlaw2MP3 command will pick all the Ulaw files from the current folder and convert them into mp3. Do you wish to continue [y/N]? " -n 1 -r
	echo
	if [[ $REPLY =~ ^[Yy]$ ]]
	then
        
        rename ulaw ul $fileName"-in.ulaw"
        rename ulaw ul $fileName"-out.ulaw"
        	
	MP3FILE=$fileName".mp3"
        sox -V3 -m -c1 -r8000 $fileName"-in.ul" -c1 -r8000 $fileName"-out.ul" -c1 -r8000 $fileName".mp3"

	if [ -e $MP3FILE ] ; then
           # echo "Merged file converted. Moving to $DIRLOC"
            # sleep 2
            DIRLOC=`echo $fileName".mp3" | cut -f3-4 -d"_" | tr . ' ' | awk '{print $1}'`               
            mkdir -p $RECDIR/$DIRLOC
            cp $MP3FILE $RECDIR/$DIRLOC/
            # echo "Moving ulaw files to safe location for future removal"
            mkdir -p $RECDIR/safe/$DIRLOC
            cp $fileName"-in.ul" "$RECDIR/safe/$DIRLOC/"
            cp $fileName"-out.ul" "$RECDIR/safe/$DIRLOC/"
	fi	        
    fi
fi
}


alias ast='asterisk -rx '

astee () {
        TODAY=`date '+%a-%d-%H-%M'`
        echo
        echo "--------------------------------------------------------------------"
        echo "Set verbosity, debug, sip debug on CLI and then wait till you have enough scrolling to analyse"
        echo "Log file will be created as /var/log/asterisk/cli-out-$TODAY.log !!!!!!!COPY IT!!!!!!!"
        echo "--------------------------------------------------------------------"
        echo
        sleep 5
        asterisk -r | tee "/var/log/asterisk/cli-out-$TODAY.log"
}


#optional - change prompt
PS1="BOX-5 \w> "
export PS1
