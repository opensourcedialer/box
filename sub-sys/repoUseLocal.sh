#!/bin/bash
cd /etc/yum.repos.d/
rename .repo .disabled-rep *.repo
cp $installRoot/setup/res/os/local.repo /etc/yum.repos.d/
sed -i "s|CHANGEME|$installRoot/sys/bin-pkgs/rpm/|g"  /etc/yum.repos.d/local.repo
yum clean all
yum clean metadata
# Decision: We could have turned on local cache for yum. No need to cache as that will just take more space for files we will already have downloaded on this system.
# Note: The file local.rep initially has only 2 // file:// because the sed along with $installRoot will add one more
