#!/bin/bash

ETHIP=`ifconfig -a | grep "inet addr:" | head -n2| tail -n1 | awk -F':' '{print $2}'| cut -d' ' -f1`
GATEWAY=""
IPADDR=""
DNS1=""
BOXURL=""
CLIENT_URL=""
USERNAME=""

echo    -e "Please enter required details:"
echo    "1) GATEWAY: "
read    GATEWAY
echo    "2) IPADDR: "
read    IPADDR
echo    "3) DNS1: "
read    DNS1
echo    "4) BOXURL=http://x.x.x.x (with the http): "
read    BOXURL
echo    "5) CLIENT_URL=http://x.aavaz.biz (with the http): "
read    CLIENT_URL
echo    "6) USERNAME: "
read    USERNAME
echo    "7) PASSWORD: "
read -s PASSWORD

URL_CONFIG=`echo $CLIENT_URL  |awk -F'//' '{print $2}'`

#-------------------------+
# System Networking
#-------------------------+

sed -i "s/GATEWAY=.*/GATEWAY="$GATEWAY"/g" /etc/sysconfig/network
sed -i "s/IPADDR=.*/IPADDR="$IPADDR"/g;s/DNS1=.*/DNS1="$DNS1"/g" /aavaz/config/ifcfg-eth0\:0
echo "nameserver $DNS1" >> /etc/resolv.conf
echo -e ""$ETHIP" aavazbox.local" >> /etc/hosts

#-------------------------+
# Managebox 
#-------------------------+

sed -ri "s/[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/$IPADDR/g" /aavaz/default/hosts/nfs/managebox/js/ivr.js
sed -ri "s/[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/$IPADDR/g" /aavaz/default/hosts/nfs/managebox/includes/js/ivr.js
sed -ri "s/[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/"$IPADDR"/g" /aavaz/default/hosts/nfs/managebox/class/model/asterisk/astmgr.php

#-------------------------+
# Asterisk
#-------------------------+
sed -i "s/BOXIP\s*=.*/BOXIP = "$IPADDR"/g" /etc/asterisk/extensions.conf
sed -i "s/BOXIP\s*=.*/BOXIP = "$IPADDR"/g" /etc/asterisk/extensions.conf

#-------------------------+
# Tomcat applications 
#-------------------------+
sed -i "s/BOX_IP\s*=.*/BOX_IP="$IPADDR"/g" /aavaz/config/box.properties

sed -i "s/ASTERISK_URL\s*=.*/ASTERISK_URL="$IPADDR"/g; s/USER_NAME=.*/USER_NAME=$USERNAME/g; s/^PASSWORD=.*/PASSWORD=$PASSWORD/g" /aavaz/box/hosts/nfs/asterisk/WEB-INF/classes/asterisk.properties
sed -i "s/ASTERISK_URL\s*=.*/ASTERISK_URL="$IPADDR"/g; s/USER_NAME=.*/USER_NAME=$USERNAME/g; s/^PASSWORD=.*/PASSWORD=$PASSWORD/g" /aavaz/box/hosts/nfs/api/WEB-INF/classes/asterisk.properties
sed -i "s/ASTERISK_URL\s*=.*/ASTERISK_URL="$IPADDR"/g" /aavaz/box/hosts/nfs/managebox/WEB-INF/classes/asterisk.properties

sed -i "s/CLIENT_URL\s*=.*/CLIENT_URL ="$URL_CONFIG"/g" /aavaz/box/hosts/nfs/asterisk/WEB-INF/classes/asterisk.properties
sed -i "s/CLIENT_URL\s*=.*/CLIENT_URL ="$URL_CONFIG"/g" /aavaz/box/hosts/nfs/api/WEB-INF/classes/asterisk.properties
sed -i "s/CLIENT_URL\s*=.*/CLIENT_URL ="$URL_CONFIG"/g" /aavaz/box/hosts/nfs/managebox/WEB-INF/classes/asterisk.properties

sed -i  "s/URL=.*/URL=http\:\/\/$URL_CONFIG/g;s/USER=.*/USER=$USERNAME/g;s/PASSWORD=.*/PASSWORD=$PASSWORD/g"      /aavaz/config/aavaz.properties
sed -i  "s/URL=.*/URL=http\:\/\/$URL_CONFIG/g;s/USER=.*/USER=$USERNAME/g;s/PASSWORD=.*/PASSWORD=$PASSWORD/g"      /aavaz/factory/config/aavaz.properties


