#!/bin/bash
A=`grep -n  "2.6.32-71.el6.i686" /boot/grub/grub.conf |grep title | head -c2`
B=`grep -n  "2.6.32-358.18.1.el6.i686" /boot/grub/grub.conf |grep title | head -c2`
C=`grep -n  "2.6.32-504.23.4.el6.i686" /boot/grub/grub.conf |grep title | head -c2`
FILE="/boot/grub/grub.conf"

if [ $A -lt $B ] && [ $B -lt $C ];then
	sed -i 's/default=.*/default=0/g' $FILE
	echo "cond1 is true"
elif [ $A -gt $B ] && [ $A -lt $C ];then
	sed -i 's/default=.*/default=1/g' $FILE
	echo "cond2 is true"
elif [ $A -gt $C ] && [ $A -lt $B ];then
	sed -i 's/default=.*/default=1/g' $FILE
	echo "cond3 is true"
elif [ $A -gt $B ] && [ $A -gt $C ];then
	sed -i 's/default=.*/default=2/g' $FILE
	echo "cond4 is true"
elif [ $A -gt $B ];then
	sed -i 's/default=.*/default=1/g' $FILE
	echo "cond5 is true"
elif [ $A -gt $C ];then
	sed -i 's/default=.*/default=1/g' $FILE
	echo "cond6 is true"
elif [ $A -lt $B ];then
	sed -i 's/default=.*/default=0/g' $FILE
	echo "cond7 is true"
elif [ $A -lt $C ];then
	sed -i 's/default=.*/default=0/g' $FILE
	echo "cond8 is true"

fi
init 6
